package com.gratus.idp.model.common;

import java.io.Serializable;

public class ChallengeEvent implements Serializable {
    private int layoutId;
    private int image;
    private String endTime;
    private String header;
    private String participants;
    private String target;
    private String timeFrame;
    private String details;

    public ChallengeEvent(int layoutId, int image, String endTime, String header, String participants, String target, String timeFrame, String details) {
        this.layoutId = layoutId;
        this.image = image;
        this.endTime = endTime;
        this.header = header;
        this.participants = participants;
        this.target = target;
        this.timeFrame = timeFrame;
        this.details = details;
    }

    public int getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(int layoutId) {
        this.layoutId = layoutId;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getParticipants() {
        return participants;
    }

    public void setParticipants(String participants) {
        this.participants = participants;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
