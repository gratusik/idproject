package com.gratus.idp.model.common;

import java.io.Serializable;
import java.util.ArrayList;

public class CommunityModel implements Serializable {
    private int layoutId;
    private ArrayList<ChallengeEvent> challengeEvent;

    private int image;
    private String header;
    private String name;
    private String miles;

    public CommunityModel(int layoutId, String header) {
        this.layoutId = layoutId;
        this.header = header;
    }

    public CommunityModel(int layoutId, ArrayList<ChallengeEvent> challengeEvent) {
        this.layoutId = layoutId;
        this.challengeEvent = challengeEvent;
    }

    public CommunityModel(int layoutId, int image, String name, String miles) {
        this.layoutId = layoutId;
        this.image = image;
        this.name = name;
        this.miles = miles;
    }

    public int getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(int layoutId) {
        this.layoutId = layoutId;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }


    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public ArrayList<ChallengeEvent> getChallengeEvent() {
        return challengeEvent;
    }

    public void setChallengeEvent(ArrayList<ChallengeEvent> challengeEvent) {
        this.challengeEvent = challengeEvent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiles() {
        return miles;
    }

    public void setMiles(String miles) {
        this.miles = miles;
    }
}
