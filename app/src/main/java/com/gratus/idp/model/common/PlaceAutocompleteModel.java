package com.gratus.idp.model.common;

import java.io.Serializable;

public class PlaceAutocompleteModel implements Serializable {
    public Integer layoutType;
    public String placeId;
    public String description;
    public String fulltext;
    public Double lat;
    public Double lon;

    public PlaceAutocompleteModel(Integer layoutType, String placeId, String description, String fulltext) {
        this.layoutType = layoutType;
        this.placeId = placeId;
        this.description = description;
        this.fulltext = fulltext;
    }

    public PlaceAutocompleteModel(Integer layoutType, String placeId, String description, String fulltext, Double lat, Double lon) {
        this.layoutType = layoutType;
        this.placeId = placeId;
        this.description = description;
        this.fulltext = fulltext;
        this.lat = lat;
        this.lon = lon;
    }

    public PlaceAutocompleteModel(Integer layoutType) {
        this.layoutType = layoutType;
    }


    public Integer getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(Integer layoutType) {
        this.layoutType = layoutType;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFulltext() {
        return fulltext;
    }

    public void setFulltext(String fulltext) {
        this.fulltext = fulltext;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }
}
