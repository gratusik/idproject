package com.gratus.idp.model.common;

import java.io.Serializable;

public class RideEarnModel implements Serializable {
    private int layoutId;
    private String euro;
    private String ride;
    private String miles;

    public RideEarnModel(int layoutId, String euro, String ride, String miles) {
        this.layoutId = layoutId;
        this.euro = euro;
        this.ride = ride;
        this.miles = miles;
    }

    public int getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(int layoutId) {
        this.layoutId = layoutId;
    }

    public String getEuro() {
        return euro;
    }

    public void setEuro(String euro) {
        this.euro = euro;
    }

    public String getRide() {
        return ride;
    }

    public void setRide(String ride) {
        this.ride = ride;
    }

    public String getMiles() {
        return miles;
    }

    public void setMiles(String miles) {
        this.miles = miles;
    }
}
