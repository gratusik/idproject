package com.gratus.idp.model.common;

import java.io.Serializable;

public class NewsFeedModel implements Serializable {
    private int layoutId;
    private int image;
    private int desc;
    private String temp;
    private String temp1;
    private int header;
    private String value;
    private String header1;
    private String value1;
    private String header2;
    public NewsFeedModel(int layoutId, int image, int desc, int header) {
        this.layoutId = layoutId;
        this.image = image;
        this.desc = desc;
        this.header = header;
    }

    public NewsFeedModel(int layoutId, String temp, String value, String header1,String temp1, String value1, String header2) {
        this.layoutId = layoutId;
        this.temp = temp;
        this.value = value;
        this.header1 = header1;
        this.temp1 = temp1;
        this.value1 = value1;
        this.header2 = header2;
    }

    public int getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(int layoutId) {
        this.layoutId = layoutId;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getDesc() {
        return desc;
    }

    public void setDesc(int desc) {
        this.desc = desc;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public int getHeader() {
        return header;
    }

    public void setHeader(int header) {
        this.header = header;
    }

    public String getTemp1() {
        return temp1;
    }

    public void setTemp1(String temp1) {
        this.temp1 = temp1;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getHeader1() {
        return header1;
    }

    public void setHeader1(String header1) {
        this.header1 = header1;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getHeader2() {
        return header2;
    }

    public void setHeader2(String header2) {
        this.header2 = header2;
    }
}
