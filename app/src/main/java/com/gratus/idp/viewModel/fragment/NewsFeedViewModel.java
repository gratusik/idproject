package com.gratus.idp.viewModel.fragment;

import android.widget.RadioGroup;

import androidx.databinding.ObservableInt;
import androidx.lifecycle.MutableLiveData;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.gratus.idp.R;
import com.gratus.idp.model.common.CyclePathNew;
import com.gratus.idp.model.common.NewsFeedModel;
import com.gratus.idp.model.request.FilterRequest;
import com.gratus.idp.model.response.FilterResponse;
import com.gratus.idp.repository.FilterRepo;
import com.gratus.idp.util.DateTimeUtil;
import com.gratus.idp.view.base.BaseViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Response;

import static com.gratus.idp.util.constants.AppConstants.NETWORK_LOST_EXP;
import static com.gratus.idp.util.constants.AppConstantsCode.NETWORK_CODE_EXP;

public class NewsFeedViewModel extends BaseViewModel {
    private ArrayList<NewsFeedModel> newsFeedModelArrayList = new ArrayList<>();
    @Inject
    public NewsFeedViewModel() {

    }

    public ArrayList<NewsFeedModel> getNewsFeedModel() {
        return newsFeedModelArrayList;
    }

    public void setNewsFeedModel(ArrayList<NewsFeedModel>  newsFeedModelArrayList) {
        this.newsFeedModelArrayList = newsFeedModelArrayList;
    }

    public void setNewsFeedItems() {
        newsFeedModelArrayList.add(new NewsFeedModel(2,"415","parts per million","CARBON DIOXIDE","2.0","F° since 1880","GLOBAL TEMPERATURE"));

        newsFeedModelArrayList.add(new NewsFeedModel(1,R.drawable.news2,R.string.news2,R.string.new2_header));
        newsFeedModelArrayList.add(new NewsFeedModel(1,R.drawable.news4,R.string.news4,R.string.new4_header));
        newsFeedModelArrayList.add(new NewsFeedModel(1,R.drawable.news5,R.string.news5,R.string.new5_header));
        newsFeedModelArrayList.add(new NewsFeedModel(1,R.drawable.news6,R.string.news6,R.string.new6_header));
        newsFeedModelArrayList.add(new NewsFeedModel(1,R.drawable.news7,R.string.news7,R.string.new7_header));
        newsFeedModelArrayList.add(new NewsFeedModel(1,R.drawable.news8,R.string.news8,R.string.new8_header));
        newsFeedModelArrayList.add(new NewsFeedModel(1,R.drawable.news9,R.string.news9,R.string.new9_header));
        newsFeedModelArrayList.add(new NewsFeedModel(1,R.drawable.news10,R.string.news10,R.string.new10_header));
        newsFeedModelArrayList.add(new NewsFeedModel(1,R.drawable.news1,R.string.news1,R.string.new1_header));
        newsFeedModelArrayList.add(new NewsFeedModel(1,R.drawable.news3,R.string.news3,R.string.new3_header));
    }
}