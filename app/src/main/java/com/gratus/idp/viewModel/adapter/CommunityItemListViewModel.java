package com.gratus.idp.viewModel.adapter;

import com.gratus.idp.model.common.CommunityModel;
import com.gratus.idp.model.common.CommunityModel;
import com.gratus.idp.view.interfaces.viewModel.CommunityLeaderItemViewModelListener;
import com.gratus.idp.view.interfaces.viewModel.NewsFeedListItemViewModelListener;

public class CommunityItemListViewModel {
    public CommunityLeaderItemViewModelListener mListener;
    private final CommunityModel communityModel;

    public CommunityItemListViewModel(CommunityModel communityModel, CommunityLeaderItemViewModelListener listener) {
        this.communityModel = communityModel;
        this.mListener = listener;
    }

    public CommunityItemListViewModel(CommunityModel communityModel) {
        this.communityModel = communityModel;
    }

    public CommunityModel getCommunityModel() {
        return communityModel;
    }

    public void onItemClick() {
        mListener.onItemClick();
    }
}
