package com.gratus.idp.viewModel.activity;

import com.gratus.idp.model.common.ChallengeEvent;
import com.gratus.idp.view.base.BaseViewModel;

import javax.inject.Inject;


public class EventViewModel extends BaseViewModel {

    private ChallengeEvent challengeEvent;

    @Inject
    public EventViewModel() {

    }

    public ChallengeEvent getChallengeEvent() {
        return challengeEvent;
    }

    public void setChallengeEventl(ChallengeEvent challengeEvent) {
        this.challengeEvent = challengeEvent;
    }

}
