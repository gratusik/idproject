package com.gratus.idp.viewModel.adapter;

import com.gratus.idp.model.common.NewsFeedModel;
import com.gratus.idp.model.common.RideEarnModel;
import com.gratus.idp.view.interfaces.viewModel.NewsFeedListItemViewModelListener;

public class RideEarnListViewModel {
    private final RideEarnModel rideEarnModel;

    public RideEarnListViewModel(RideEarnModel rideEarnModels) {
        this.rideEarnModel = rideEarnModels;
    }

    public RideEarnModel getRideEarnModel() {
        return rideEarnModel;
    }

}
