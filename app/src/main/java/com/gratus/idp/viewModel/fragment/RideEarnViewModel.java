package com.gratus.idp.viewModel.fragment;

import com.gratus.idp.R;
import com.gratus.idp.model.common.ChallengeEvent;
import com.gratus.idp.model.common.RideEarnModel;
import com.gratus.idp.model.common.RideEarnModel;
import com.gratus.idp.view.base.BaseViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

public class RideEarnViewModel extends BaseViewModel {
    private ArrayList<RideEarnModel> rideEarnModels = new ArrayList<>();
    @Inject
    public RideEarnViewModel() {

    }

    public ArrayList<RideEarnModel> getRideEarnModels() {
        return rideEarnModels;
    }

    public void setRideEarnModels(ArrayList<RideEarnModel>  communityModel) {
        this.rideEarnModels = communityModel;
    }

    public void setNewsFeedItems() {
        rideEarnModels.add(new RideEarnModel(1,"$4.50","30-10-2020","3 Miles"));
        rideEarnModels.add(new RideEarnModel(1,"$3.00","29-10-2020","2 Miles"));
        rideEarnModels.add(new RideEarnModel(1,"$1.50","28-10-2020","1 Miles"));
        rideEarnModels.add(new RideEarnModel(1,"$0.00","27-10-2020","0 Miles"));
    }
}