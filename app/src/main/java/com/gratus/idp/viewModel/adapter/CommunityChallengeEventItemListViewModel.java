package com.gratus.idp.viewModel.adapter;

import com.gratus.idp.model.common.ChallengeEvent;
import com.gratus.idp.model.common.ChallengeEvent;
import com.gratus.idp.view.interfaces.viewModel.CommunityCEItemViewModelListener;
import com.gratus.idp.view.interfaces.viewModel.CommunityLeaderItemViewModelListener;

public class CommunityChallengeEventItemListViewModel {
    public CommunityCEItemViewModelListener mListener;
    private final ChallengeEvent challengeEvent;

    public CommunityChallengeEventItemListViewModel(ChallengeEvent challengeEvent, CommunityCEItemViewModelListener listener) {
        this.challengeEvent = challengeEvent;
        this.mListener = listener;
    }

    public CommunityChallengeEventItemListViewModel(ChallengeEvent challengeEvent) {
        this.challengeEvent = challengeEvent;
    }

    public ChallengeEvent getChallengeEvent() {
        return challengeEvent;
    }

    public void onItemClick() {
        mListener.onItemClick();
    }
}
