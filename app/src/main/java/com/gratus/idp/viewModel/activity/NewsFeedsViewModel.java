package com.gratus.idp.viewModel.activity;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.gratus.idp.model.common.CyclePathNew;
import com.gratus.idp.model.common.NewsFeedModel;
import com.gratus.idp.model.request.PathRequest;
import com.gratus.idp.model.response.PathResponse;
import com.gratus.idp.repository.PathRepo;
import com.gratus.idp.view.base.BaseViewModel;

import java.io.StringReader;

import javax.inject.Inject;


public class NewsFeedsViewModel extends BaseViewModel {

    private NewsFeedModel newsFeedModel;

    @Inject
    public NewsFeedsViewModel() {

    }

    public NewsFeedModel getNewsFeedModel() {
        return newsFeedModel;
    }

    public void setNewsFeedModel(NewsFeedModel newsFeedModel) {
        this.newsFeedModel = newsFeedModel;
    }

}
