package com.gratus.idp.viewModel.activity;

import com.gratus.idp.R;
import com.gratus.idp.model.common.ChallengeEvent;
import com.gratus.idp.model.common.CommunityModel;
import com.gratus.idp.view.base.BaseViewModel;

import java.util.ArrayList;

import javax.inject.Inject;


public class LeaderViewModel extends BaseViewModel {

    private ArrayList<CommunityModel> communityModels = new ArrayList<>();

    @Inject
    public LeaderViewModel() {

    }

    public ArrayList<CommunityModel> getCommunityModels() {
        return communityModels;
    }

    public void setCommunityModels(ArrayList<CommunityModel> communityModel) {
        this.communityModels = communityModel;
    }

    public void setNewsFeedItems() {
        communityModels.add(new CommunityModel(2, R.drawable.user, "Gratus Iruthaya kisho", "250 miles"));
        communityModels.add(new CommunityModel(2,R.drawable.user,"Rasin","235  miles"));
        communityModels.add(new CommunityModel(2,R.drawable.user,"Jayanth","200  miles"));
        communityModels.add(new CommunityModel(2,R.drawable.user,"Romeo","160  miles"));
        communityModels.add(new CommunityModel(2,R.drawable.user,"Davis","140  miles"));
        communityModels.add(new CommunityModel(2,R.drawable.user,"Michele","115  miles"));
        communityModels.add(new CommunityModel(2,R.drawable.user,"Da vinci","100  miles"));
        communityModels.add(new CommunityModel(2,R.drawable.user,"Newton","70  miles"));
        communityModels.add(new CommunityModel(2,R.drawable.user,"Anushka","20  miles"));
    }
}
