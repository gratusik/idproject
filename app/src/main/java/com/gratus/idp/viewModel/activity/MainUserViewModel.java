package com.gratus.idp.viewModel.activity;

import androidx.lifecycle.MutableLiveData;

import com.gratus.idp.model.request.LoginRequest;
import com.gratus.idp.model.response.LoginResponse;
import com.gratus.idp.repository.LoginRepo;
import com.gratus.idp.view.base.BaseViewModel;

import javax.inject.Inject;


public class MainUserViewModel extends BaseViewModel {

    @Inject
    public MainUserViewModel() {

    }
}
