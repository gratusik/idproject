package com.gratus.idp.viewModel.activity;

import com.gratus.idp.model.common.ChallengeEvent;
import com.gratus.idp.model.common.NewsFeedModel;
import com.gratus.idp.view.base.BaseViewModel;

import javax.inject.Inject;


public class ChallengeViewModel extends BaseViewModel {

    private ChallengeEvent challengeEvent;

    @Inject
    public ChallengeViewModel() {

    }

    public ChallengeEvent getChallengeEvent() {
        return challengeEvent;
    }

    public void setChallengeEventl(ChallengeEvent challengeEvent) {
        this.challengeEvent = challengeEvent;
    }

}
