package com.gratus.idp.viewModel.fragment;

import com.gratus.idp.R;
import com.gratus.idp.model.common.ChallengeEvent;
import com.gratus.idp.model.common.CommunityModel;
import com.gratus.idp.model.common.NewsFeedModel;
import com.gratus.idp.view.base.BaseViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

public class CommunityViewModel extends BaseViewModel {
    private ArrayList<CommunityModel> communityModels = new ArrayList<>();
    @Inject
    public CommunityViewModel() {

    }

    public ArrayList<CommunityModel> getCommunityModels() {
        return communityModels;
    }

    public void setCommunityModels(ArrayList<CommunityModel>  communityModel) {
        this.communityModels = communityModel;
    }

    public void setNewsFeedItems() {
        communityModels.add(new CommunityModel(1,"Challenges"));
        ArrayList<ChallengeEvent> challenge = new ArrayList<>();
        challenge.add(new ChallengeEvent(1,R.drawable.challenge1,"ENDS IN 19 HOURS","YOUR MONTLY 30KM","10,023  participants","30Km","1 Oct - 1 Nov","Starting and finishing on the outskirts of Cambridge, the 35 and 60 mile routes will take you through some of Cambridgeshire's most beautiful countryside. Highlights include Wicken Fen Nature Reserve on the shorter route and Ely Cathedral on the 60 miler. Plenty of refreshment stops along the way makes this a great day's cycling with the opportunity to raise sponsorship for your chosen charity."));
        challenge.add(new ChallengeEvent(1,R.drawable.challenge2,"ENDS IN 48 HOURS","YOUR MONTLY 10KM","9,067  participants","10Km","1 Oct - 2 Nov","It's a Spring ride for 2021 and a new route as well! Details on that coming soon....\n" +
                "\n" +
                "The Christie Manchester 100km will take you through beautiful country lanes that start and finish at Wythenshawe Park and that sweep through the stunning Cheshire landscape. Cyclists will return to a warm welcome by Team Christie.\n" +
                "\n" +
                "Money raised from The Christie Manchester 100km helps to provide inspiration and hope to cancer patients and their families\n"));
        communityModels.add(new CommunityModel(3,challenge));
        communityModels.add(new CommunityModel(1,"Events"));
        ArrayList<ChallengeEvent> events = new ArrayList<>();
        events.add(new ChallengeEvent(2,R.drawable.event1,"ENDS IN 19 HOURS","Bike Skills 101 -- Fundamental Bike Handling Skills","10,023  participants","10Km","1 Oct - 1 Nov","Bike Skills 201 is a continuation of what you’ve learned in Bike Skills 101. In this 4-hour co-ed clinic, we’ll teach you how to climb like a pro – seated climbs, standing climbs, short climbs, steep climbs, extended climbs. And then, we’ll teach you how to come back down again, focusing on a fast straight descent, and then a technical switchbacky descent. Pre-requisite: Bike Skills 101"));
        events.add(new ChallengeEvent(2,R.drawable.event2,"ENDS IN 10 DAYS","Bike Skills 201 -- Climbing + Descending Skills","10,5553  participants","20Km","1 Nov - 10 Nov","Starting and finishing on the outskirts of Cambridge, the 35 and 60 mile routes will take you through some of Cambridgeshire's most beautiful countryside. Highlights include Wicken Fen Nature Reserve on the shorter route and Ely Cathedral on the 60 miler. Plenty of refreshment stops along the way makes this a great day's cycling with the opportunity to raise sponsorship for your chosen charity."));
        events.add(new ChallengeEvent(2,R.drawable.event3,"ENDS IN 20 DAYS","Cycling in Holland is a delight - the bike always comes first, here!","16,023  participants","30Km","10 Nov - 20 Nov","Following the traffic-free cycle paths and quiet roads that criss-cross the country, this hostel-based cycling holiday will introduce you to the lesser-known landscapes of the Netherlands as we ride alongside tree-lined canals and atop dykes, exploring the pretty villages and natural beauty of the Dutch countryside.\n" +
                "\n" +
                "Visiting the beaches, castles, historic cities and nature reserves of this cyclists' paradise, you'll experience the pleasure and relaxation that comes with a holiday in a country where the cyclist is king...and queen.\n" +
                "\n" +
                "Travelling Monday 26th July, the overnight ferry crossing onboard Stena Line's 'Superferry', Hollandia, with its comfortable cabins, two restaurants, two bars and a cinema, provides a great onboard experience meaning you're fresh for the start of this great cycling holiday. Following signed routes through picturesque and varied countryside, you'll be welcomed with typically warm Dutch hospitality at the beautiful, clean and friendly hostels.\n"));
        communityModels.add(new CommunityModel(3,events));
        communityModels.add(new CommunityModel(1,"LeaderBoard"));
        communityModels.add(new CommunityModel(2,R.drawable.user,"Gratus Iruthaya kisho","250  miles"));
    }
}