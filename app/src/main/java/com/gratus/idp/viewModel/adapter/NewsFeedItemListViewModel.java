package com.gratus.idp.viewModel.adapter;

import com.gratus.idp.model.common.CyclePathNew;
import com.gratus.idp.model.common.NewsFeedModel;
import com.gratus.idp.view.interfaces.adapter.NewsFeedListListener;
import com.gratus.idp.view.interfaces.viewModel.NewsFeedListItemViewModelListener;
import com.gratus.idp.view.interfaces.viewModel.ReportListItemViewModelListener;

public class NewsFeedItemListViewModel {
    public NewsFeedListItemViewModelListener mListener;
    private final NewsFeedModel newsFeedModel;

    public NewsFeedItemListViewModel(NewsFeedModel newsFeedModel, NewsFeedListItemViewModelListener listener) {
        this.newsFeedModel = newsFeedModel;
        this.mListener = listener;
    }

    public NewsFeedItemListViewModel(NewsFeedModel newsFeedModel) {
        this.newsFeedModel = newsFeedModel;
    }

    public NewsFeedModel getNewsFeedModel() {
        return newsFeedModel;
    }

    public void onItemClick() {
        mListener.onItemClick();
    }
}
