package com.gratus.idp.di.modules;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.gratus.idp.view.adapter.NewsFeedAdapter;
import com.gratus.idp.view.adapter.RideEarnAdapter;
import com.gratus.idp.view.fragment.NewsFeedFragment;
import com.gratus.idp.view.fragment.RideEarnFragment;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class RideEarnAdapterModule {
    @Provides
    RideEarnAdapter provideRideEarnAdapter() {
        return new RideEarnAdapter(new ArrayList<>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(RideEarnFragment fragment) {
        return new LinearLayoutManager(fragment.getActivity());
    }
}
