package com.gratus.idp.di.modules;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.gratus.idp.di.key.ViewModelKey;
import com.gratus.idp.viewModel.activity.ChallengeViewModel;
import com.gratus.idp.viewModel.activity.EditProfileViewModel;
import com.gratus.idp.viewModel.activity.EventViewModel;
import com.gratus.idp.viewModel.activity.LeaderViewModel;
import com.gratus.idp.viewModel.activity.LoginViewModel;
import com.gratus.idp.viewModel.activity.MainUserViewModel;
import com.gratus.idp.viewModel.activity.MainViewModel;
import com.gratus.idp.viewModel.activity.NewsFeedsViewModel;
import com.gratus.idp.viewModel.activity.PathViewModel;
import com.gratus.idp.viewModel.activity.ProfileViewModel;
import com.gratus.idp.viewModel.activity.ResetPasswordViewModel;
import com.gratus.idp.viewModel.activity.SignUpViewModel;
import com.gratus.idp.viewModel.activity.SplashViewModel;
import com.gratus.idp.di.factory.ViewModelFactory;
import com.gratus.idp.viewModel.adapter.RideEarnListViewModel;
import com.gratus.idp.viewModel.fragment.CommunityViewModel;
import com.gratus.idp.viewModel.fragment.HomeViewModel;
import com.gratus.idp.viewModel.fragment.NavigationViewModel;
import com.gratus.idp.viewModel.fragment.NewsFeedViewModel;
import com.gratus.idp.viewModel.fragment.ReportListViewModel;
import com.gratus.idp.viewModel.fragment.RideEarnViewModel;
import com.gratus.idp.viewModel.fragment.SettingsViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    abstract ViewModel bindLoginViewModel(LoginViewModel loginViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel.class)
    abstract ViewModel bindSplashViewModel(SplashViewModel splashViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(ResetPasswordViewModel.class)
    abstract ViewModel bindResetPasswordViewModel(ResetPasswordViewModel resetPasswordViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(SignUpViewModel.class)
    abstract ViewModel bindSignUpViewModel(SignUpViewModel signUpViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel.class)
    abstract ViewModel bindProfileViewModel(ProfileViewModel profileViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(EditProfileViewModel.class)
    abstract ViewModel bindEditProfileViewModel(EditProfileViewModel editProfileViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel mainViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel.class)
    abstract ViewModel bindHomeViewModel(HomeViewModel homeViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(ReportListViewModel.class)
    abstract ViewModel bindReportViewModel(ReportListViewModel reportListViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel.class)
    abstract ViewModel bindSettingsViewModel(SettingsViewModel settingsViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(PathViewModel.class)
    abstract ViewModel bindPathViewModel(PathViewModel pathViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(MainUserViewModel.class)
    abstract ViewModel bindMainUserViewModel(MainUserViewModel mainUserViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(NewsFeedViewModel.class)
    abstract ViewModel bindNewsFeedViewModel(NewsFeedViewModel newsFeedViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(NewsFeedsViewModel.class)
    abstract ViewModel bindNewsFeedsViewModel(NewsFeedsViewModel newsFeedViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(CommunityViewModel.class)
    abstract ViewModel bindCommunityViewModel(CommunityViewModel communityViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(ChallengeViewModel.class)
    abstract ViewModel bindChallengeViewModel(ChallengeViewModel challengeViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(EventViewModel.class)
    abstract ViewModel bindEventViewModell(EventViewModel eventViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(LeaderViewModel.class)
    abstract ViewModel bindLeaderViewModel(LeaderViewModel leaderViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(RideEarnViewModel.class)
    abstract ViewModel bindRideEarnViewModel(RideEarnViewModel rideEarnViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(NavigationViewModel.class)
    abstract ViewModel bindNavigationViewModel(NavigationViewModel navigationViewModel);
    @Binds
    abstract ViewModelProvider.Factory bindFactory(ViewModelFactory factory);

}