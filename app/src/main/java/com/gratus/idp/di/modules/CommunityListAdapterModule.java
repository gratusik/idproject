package com.gratus.idp.di.modules;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.gratus.idp.view.adapter.CommunityAdapter;
import com.gratus.idp.view.adapter.ReportListAdapter;
import com.gratus.idp.view.fragment.CommunityFragment;
import com.gratus.idp.view.fragment.ReportListFragment;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class CommunityListAdapterModule {
    @Provides
    CommunityAdapter provideCommunityListAdapter() {
        return new CommunityAdapter(new ArrayList<>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(CommunityFragment fragment) {
        return new LinearLayoutManager(fragment.getActivity());
    }
}
