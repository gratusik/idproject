package com.gratus.idp.di.modules;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.gratus.idp.view.adapter.NewsFeedAdapter;
import com.gratus.idp.view.fragment.NewsFeedFragment;
import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class NewsFeedAdapterModule {
    @Provides
    NewsFeedAdapter provideNewsFeedAdapter() {
        return new NewsFeedAdapter(new ArrayList<>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(NewsFeedFragment fragment) {
        return new LinearLayoutManager(fragment.getActivity());
    }
}
