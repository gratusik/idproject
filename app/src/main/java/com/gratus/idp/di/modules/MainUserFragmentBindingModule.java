package com.gratus.idp.di.modules;

import com.gratus.idp.view.adapter.ChallengeEventAdapter;
import com.gratus.idp.view.adapter.CommunityAdapter;
import com.gratus.idp.view.adapter.RideEarnAdapter;
import com.gratus.idp.view.fragment.CommunityFragment;
import com.gratus.idp.view.fragment.NavigationFragment;
import com.gratus.idp.view.fragment.NewsFeedFragment;
import com.gratus.idp.view.fragment.ReportListFragment;
import com.gratus.idp.view.fragment.RideEarnFragment;
import com.gratus.idp.view.fragment.SettingsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
@Module
public abstract class MainUserFragmentBindingModule {
    @ContributesAndroidInjector(modules = NewsFeedAdapterModule.class)
    abstract NewsFeedFragment provideNewsFeedFragment();
    @ContributesAndroidInjector(modules = {CommunityListAdapterModule.class})
    abstract CommunityFragment provideCommunityFragmentt();
    @ContributesAndroidInjector(modules = {RideEarnAdapterModule.class})
    abstract RideEarnFragment provideRideEarnFragment();
    @ContributesAndroidInjector
    abstract NavigationFragment provideNavigationFragment();
    @ContributesAndroidInjector
    abstract SettingsFragment provideSettingsFragment();
}
