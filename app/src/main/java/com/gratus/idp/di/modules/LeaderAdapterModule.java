package com.gratus.idp.di.modules;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.gratus.idp.view.activity.LeaderActivity;
import com.gratus.idp.view.adapter.LeaderAdapter;
import com.gratus.idp.view.adapter.NewsFeedAdapter;
import com.gratus.idp.view.fragment.NewsFeedFragment;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class LeaderAdapterModule {
    @Provides
    LeaderAdapter provideLeaderAdapter() {
        return new LeaderAdapter(new ArrayList<>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(LeaderActivity fragment) {
        return new LinearLayoutManager(fragment);
    }
}
