package com.gratus.idp.view.activity;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.gratus.idp.R;
import com.gratus.idp.databinding.ActivityNewsfeedListBinding;
import com.gratus.idp.model.common.NewsFeedModel;
import com.gratus.idp.view.base.BaseActivity;
import com.gratus.idp.viewModel.activity.NewsFeedsViewModel;

import javax.inject.Inject;

public class NewsFeedActivity extends BaseActivity {
    private ActivityNewsfeedListBinding activityNewsfeedListBinding;
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private NewsFeedsViewModel newsFeedViewModel;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityNewsfeedListBinding = DataBindingUtil.setContentView(this, R.layout.activity_newsfeed_list);
        //  ((BaseApplication) getApplicationContext()).getAppComponent().inject(this);
        newsFeedViewModel = ViewModelProviders.of(this, viewModelFactory).get(NewsFeedsViewModel.class);
        activityNewsfeedListBinding.setNewsFeedViewModel(newsFeedViewModel);
        activityNewsfeedListBinding.setLifecycleOwner(this);
        activityNewsfeedListBinding.backArrowImg.setOnClickListener(v -> onBackPressed());
        newsFeedViewModel.setNewsFeedModel((NewsFeedModel) getIntent().getSerializableExtra("newsFeedModel"));
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected, activityNewsfeedListBinding.parent);
        setIntial(false);
    }
}
