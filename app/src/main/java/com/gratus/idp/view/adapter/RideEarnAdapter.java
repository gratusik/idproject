package com.gratus.idp.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gratus.idp.databinding.ItemNewsfeedListBinding;
import com.gratus.idp.databinding.ItemRideEarnBinding;
import com.gratus.idp.databinding.ItemTempNewsfeedListBinding;
import com.gratus.idp.model.common.RideEarnModel;
import com.gratus.idp.model.common.RideEarnModel;
import com.gratus.idp.view.base.BaseViewHolder;
import com.gratus.idp.view.fragment.NewsFeedFragment;
import com.gratus.idp.view.interfaces.adapter.NewsFeedListListener;
import com.gratus.idp.view.interfaces.viewModel.NewsFeedListItemViewModelListener;
import com.gratus.idp.viewModel.adapter.NewsFeedItemListViewModel;
import com.gratus.idp.viewModel.adapter.RideEarnListViewModel;

import java.util.ArrayList;

public class RideEarnAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_NORMAL = 1;
    private NewsFeedListListener mListener;
    private ArrayList<RideEarnModel> rideEarnModels;

    public RideEarnAdapter(ArrayList<RideEarnModel> rideEarnModels) {
        this.rideEarnModels = rideEarnModels;
    }

    public void setmListener(NewsFeedFragment mListener) {
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                 ItemRideEarnBinding itemRideEarnBinding = ItemRideEarnBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new RideEarnListViewHolder(itemRideEarnBinding);
            default:
                itemRideEarnBinding = ItemRideEarnBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new RideEarnListViewHolder(itemRideEarnBinding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
            return rideEarnModels.size();
    }

    @Override
    public int getItemViewType(int position) {
            return rideEarnModels.get(position).getLayoutId();
    }

    public void clearItems() {
        rideEarnModels.clear();
    }

    public void addItems(ArrayList<RideEarnModel> rideEarnModel) {
        rideEarnModels.clear();
        rideEarnModels.addAll(rideEarnModel);
        notifyDataSetChanged();
    }

    private class RideEarnListViewHolder extends BaseViewHolder {
        private ItemRideEarnBinding mBinding;
        private RideEarnListViewModel rideEarnListViewModel;

        public RideEarnListViewHolder(ItemRideEarnBinding itemRideEarnBinding) {
            super(itemRideEarnBinding.getRoot());
            this.mBinding = itemRideEarnBinding;
        }

        @Override
        public void onBind(int position) {
            final RideEarnModel rideEarnModel = rideEarnModels.get(position);
            rideEarnListViewModel = new RideEarnListViewModel(rideEarnModel);
            mBinding.setRideEarnListViewModel(rideEarnListViewModel);
        }
    }
}
