package com.gratus.idp.view.interfaces.adapter;

public interface AutoCompleteItemInterface {
    void onPlaceItemClick(int adapterPosition);
}
