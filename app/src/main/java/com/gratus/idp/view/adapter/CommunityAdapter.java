package com.gratus.idp.view.adapter;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gratus.idp.databinding.ItemChallengeEventBinding;
import com.gratus.idp.databinding.ItemCommunityHeaderBinding;
import com.gratus.idp.databinding.ItemCommunityLeaderboardBinding;
import com.gratus.idp.databinding.ItemNewsfeedListBinding;
import com.gratus.idp.databinding.ItemTempNewsfeedListBinding;
import com.gratus.idp.model.common.CommunityModel;
import com.gratus.idp.model.common.NewsFeedModel;
import com.gratus.idp.view.base.BaseViewHolder;
import com.gratus.idp.view.fragment.CommunityFragment;
import com.gratus.idp.view.fragment.NewsFeedFragment;
import com.gratus.idp.view.interfaces.adapter.CommunityListListener;
import com.gratus.idp.view.interfaces.adapter.NewsFeedListListener;
import com.gratus.idp.view.interfaces.viewModel.CommunityLeaderItemViewModelListener;
import com.gratus.idp.view.interfaces.viewModel.NewsFeedListItemViewModelListener;
import com.gratus.idp.viewModel.adapter.CommunityItemListViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

public class CommunityAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_HEADER = 1;
    public static final int VIEW_TYPE_LEADER = 2;
    public static final int VIEW_TYPE_CHALLENGE_EVENT = 3;
    private CommunityListListener mListener;
    @Inject
    Context context;
    private ArrayList<CommunityModel> communityModels;

    public CommunityAdapter(ArrayList<CommunityModel> communityModels) {
        this.communityModels = communityModels;
    }

    public void setmListener(CommunityFragment mListener) {
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                 ItemCommunityHeaderBinding itemCommunityHeaderBinding = ItemCommunityHeaderBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new CommunityHeaderViewHolder(itemCommunityHeaderBinding);
            case VIEW_TYPE_LEADER:
                ItemCommunityLeaderboardBinding itemCommunityLeaderboardBinding = ItemCommunityLeaderboardBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new CommunityLeaderViewHolder(itemCommunityLeaderboardBinding);
            case VIEW_TYPE_CHALLENGE_EVENT:
                ItemChallengeEventBinding itemChallengeEventBinding = ItemChallengeEventBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new CommunityChallengeEventViewHolder(itemChallengeEventBinding);
            default:
                 itemCommunityHeaderBinding = ItemCommunityHeaderBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new CommunityHeaderViewHolder(itemCommunityHeaderBinding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
            return communityModels.size();
    }

    @Override
    public int getItemViewType(int position) {
            return communityModels.get(position).getLayoutId();
    }

    public void clearItems() {
        communityModels.clear();
    }

    public void addItems(ArrayList<CommunityModel> communityModel) {
        communityModels.clear();
        communityModels.addAll(communityModel);
        notifyDataSetChanged();
    }

    private class CommunityHeaderViewHolder extends BaseViewHolder{
        private ItemCommunityHeaderBinding mBinding;
        private CommunityItemListViewModel communityItemListViewModel;

        public CommunityHeaderViewHolder(ItemCommunityHeaderBinding itemCommunityHeaderBinding) {
            super(itemCommunityHeaderBinding.getRoot());
            this.mBinding = itemCommunityHeaderBinding;
        }

        @Override
        public void onBind(int position) {
            final CommunityModel communityModel = communityModels.get(position);
            communityItemListViewModel = new CommunityItemListViewModel(communityModel);
            mBinding.setCommunityItemListViewModel(communityItemListViewModel);
        }

    }

    private class CommunityLeaderViewHolder extends BaseViewHolder  implements CommunityLeaderItemViewModelListener {
        private ItemCommunityLeaderboardBinding mBinding;
        private CommunityItemListViewModel communityItemListViewModel;

        public CommunityLeaderViewHolder(ItemCommunityLeaderboardBinding itemCommunityLeaderboardBinding) {
            super(itemCommunityLeaderboardBinding.getRoot());
            this.mBinding = itemCommunityLeaderboardBinding;
        }

        @Override
        public void onBind(int position) {
            final CommunityModel communityModel = communityModels.get(position);
            communityItemListViewModel = new CommunityItemListViewModel(communityModel, this);
            mBinding.setCommunityItemListViewModel(communityItemListViewModel);
        }

        @Override
        public void onItemClick() {
            mListener.onItemClick(communityModels.get(getAdapterPosition()));
        }
    }

    private class CommunityChallengeEventViewHolder extends BaseViewHolder {
        private ItemChallengeEventBinding mBinding;
        private CommunityItemListViewModel communityItemListViewModel;

        public CommunityChallengeEventViewHolder(ItemChallengeEventBinding itemChallengeEventBinding) {
            super(itemChallengeEventBinding.getRoot());
            this.mBinding = itemChallengeEventBinding;
        }

        @Override
        public void onBind(int position) {
            final CommunityModel communityModel = communityModels.get(position);
            mBinding.setCommunityItemListViewModel(communityItemListViewModel);
            mBinding.recView.setAdapter(new ChallengeEventAdapter(mListener,communityModel.getChallengeEvent()));
            mBinding.recView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
            mBinding.recView.setItemAnimator(new DefaultItemAnimator());
            communityItemListViewModel = new CommunityItemListViewModel(communityModel);
        }
    }
}
