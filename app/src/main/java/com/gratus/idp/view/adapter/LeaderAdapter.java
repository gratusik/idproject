package com.gratus.idp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gratus.idp.databinding.ItemChallengeEventBinding;
import com.gratus.idp.databinding.ItemCommunityHeaderBinding;
import com.gratus.idp.databinding.ItemCommunityLeaderboardBinding;
import com.gratus.idp.model.common.CommunityModel;
import com.gratus.idp.view.base.BaseViewHolder;
import com.gratus.idp.view.fragment.CommunityFragment;
import com.gratus.idp.view.interfaces.adapter.CommunityListListener;
import com.gratus.idp.view.interfaces.viewModel.CommunityLeaderItemViewModelListener;
import com.gratus.idp.viewModel.adapter.CommunityItemListViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

public class LeaderAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_LEADER = 2;
    private CommunityListListener mListener;
    @Inject
    Context context;
    private ArrayList<CommunityModel> communityModels;

    public LeaderAdapter(ArrayList<CommunityModel> communityModels) {
        this.communityModels = communityModels;
    }

    public void setmListener(CommunityFragment mListener) {
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_LEADER:
                ItemCommunityLeaderboardBinding itemCommunityLeaderboardBinding = ItemCommunityLeaderboardBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new CommunityLeaderViewHolder(itemCommunityLeaderboardBinding);
            default:
                itemCommunityLeaderboardBinding = ItemCommunityLeaderboardBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new CommunityLeaderViewHolder(itemCommunityLeaderboardBinding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return communityModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return communityModels.get(position).getLayoutId();
    }

    public void clearItems() {
        communityModels.clear();
    }

    public void addItems(ArrayList<CommunityModel> communityModel) {
        communityModels.clear();
        communityModels.addAll(communityModel);
        notifyDataSetChanged();
    }


    private class CommunityLeaderViewHolder extends BaseViewHolder {
        private ItemCommunityLeaderboardBinding mBinding;
        private CommunityItemListViewModel communityItemListViewModel;

        public CommunityLeaderViewHolder(ItemCommunityLeaderboardBinding itemCommunityLeaderboardBinding) {
            super(itemCommunityLeaderboardBinding.getRoot());
            this.mBinding = itemCommunityLeaderboardBinding;
        }

        @Override
        public void onBind(int position) {
            final CommunityModel communityModel = communityModels.get(position);
            communityItemListViewModel = new CommunityItemListViewModel(communityModel);
            mBinding.setCommunityItemListViewModel(communityItemListViewModel);
        }
    }
}
