package com.gratus.idp.view.interfaces.viewModel;

public interface NewsFeedListItemViewModelListener {
    void onItemClick();
}
