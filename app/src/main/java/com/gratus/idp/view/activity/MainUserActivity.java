package com.gratus.idp.view.activity;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.gratus.idp.R;
import com.gratus.idp.databinding.ActivityMainBinding;
import com.gratus.idp.databinding.ActivityUserMainBinding;
import com.gratus.idp.view.base.BaseActivity;
import com.gratus.idp.view.fragment.CommunityFragment;
import com.gratus.idp.view.fragment.HomeFragment;
import com.gratus.idp.view.fragment.NavigationFragment;
import com.gratus.idp.view.fragment.NewsFeedFragment;
import com.gratus.idp.view.fragment.ReportListFragment;
import com.gratus.idp.view.fragment.RideEarnFragment;
import com.gratus.idp.view.fragment.SettingsFragment;
import com.gratus.idp.viewModel.activity.MainUserViewModel;
import com.gratus.idp.viewModel.activity.MainViewModel;

import javax.inject.Inject;

import static com.gratus.idp.util.constants.AppConstants.FRAGMENT_HOME;
import static com.gratus.idp.util.constants.AppConstants.FRAGMENT_OTHER;


public class MainUserActivity extends BaseActivity  {

    private ActivityUserMainBinding activityUserMainBinding;
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private MainUserViewModel mainUserViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityUserMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_main);
//        ((BaseApplication) getApplicationContext()).getAppComponent()
//                .inject(BaseApplication)
//                .inject(this);
        mainUserViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainUserViewModel.class);
        activityUserMainBinding.setMainViewModel(mainUserViewModel);
        activityUserMainBinding.setLifecycleOwner(this);
        viewFragment(new NewsFeedFragment(),FRAGMENT_HOME);
        setupNavMenu();
    }

    private void setupNavMenu() {
        activityUserMainBinding.bottomNavigation.setOnNavigationItemSelectedListener(item -> {
           switch (item.getItemId()) {
               case R.id.news_feeds:
                   viewFragment(new NewsFeedFragment(), FRAGMENT_HOME);
                   return true;
               case R.id.community:
                   viewFragment(new CommunityFragment(), FRAGMENT_OTHER);
                   return true;
               case R.id.navigation:
                   viewFragment(new NavigationFragment(), FRAGMENT_OTHER);
                   return true;
               case R.id.ride_earn:
                   viewFragment(new RideEarnFragment(), FRAGMENT_OTHER);
                   return true;
               case R.id.settings:
                   viewFragment(new SettingsFragment(), FRAGMENT_OTHER);
                   return true;
           }
           return false;
       });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected, activityUserMainBinding.parent);
        setIntial(false);
    }

    private void viewFragment(Fragment fragment, String name){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container,fragment);
        /*if(name.equals( FRAGMENT_OTHER) ) {
            fragmentTransaction.sta
            fragmentTransaction.addToBackStack(name);
        }*/
        fragmentTransaction.commit();
    }
}