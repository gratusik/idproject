package com.gratus.idp.view.interfaces.adapter;

import com.gratus.idp.model.common.CyclePathNew;
import com.gratus.idp.model.common.NewsFeedModel;

public interface NewsFeedListListener {
    void onItemClick(NewsFeedModel position);
}
