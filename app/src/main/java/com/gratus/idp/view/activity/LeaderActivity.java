package com.gratus.idp.view.activity;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.gratus.idp.R;
import com.gratus.idp.databinding.ActivityEventBinding;
import com.gratus.idp.databinding.ActivityLeaderBinding;
import com.gratus.idp.model.common.ChallengeEvent;
import com.gratus.idp.view.adapter.CommunityAdapter;
import com.gratus.idp.view.adapter.LeaderAdapter;
import com.gratus.idp.view.base.BaseActivity;
import com.gratus.idp.viewModel.activity.EventViewModel;
import com.gratus.idp.viewModel.activity.LeaderViewModel;

import javax.inject.Inject;

public class LeaderActivity extends BaseActivity {
    private ActivityLeaderBinding activityLeaderBinding;
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private LeaderViewModel leaderViewModel;

    @Inject
    LeaderAdapter leaderAdapter;
    @Inject
    LinearLayoutManager mLayoutManager;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityLeaderBinding = DataBindingUtil.setContentView(this, R.layout.activity_leader);
        //  ((BaseApplication) getApplicationContext()).getAppComponent().inject(this);
        leaderViewModel = ViewModelProviders.of(this, viewModelFactory).get(LeaderViewModel.class);
        activityLeaderBinding.setLeaderViewModel(leaderViewModel);
        activityLeaderBinding.setLifecycleOwner(this);
        activityLeaderBinding.backArrowImg.setOnClickListener(v -> onBackPressed());
        leaderViewModel.setNewsFeedItems();
        setNewsUp();
        updateReport();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected, activityLeaderBinding.parent);
        setIntial(false);
    }

    private void setNewsUp() {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        activityLeaderBinding.recView.setLayoutManager(mLayoutManager);
        activityLeaderBinding.recView.setItemAnimator(new DefaultItemAnimator());
        activityLeaderBinding.recView.setAdapter(leaderAdapter);
    }


    private void updateReport() {
        leaderAdapter.addItems(leaderViewModel.getCommunityModels());
    }

}
