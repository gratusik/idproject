package com.gratus.idp.view.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gratus.idp.R;
import com.gratus.idp.model.common.PlaceAutocompleteModel;
import com.gratus.idp.view.fragment.NavigationFragment;
import com.gratus.idp.view.fragment.NewsFeedFragment;
import com.gratus.idp.view.interfaces.adapter.AutoCompleteItemInterface;

import java.util.ArrayList;

public class PlaceAutocompleteAdapter extends RecyclerView.Adapter{
    private int GPS = 3;
    private int SEARCH = 4;
    private int SEPARATORLINE = 7;

    private Context mContext;
    private ArrayList<PlaceAutocompleteModel> placeAutocompleteModels = new ArrayList<>();
    private AutoCompleteItemInterface autoCompleteItemInterface;

    private SeparatorLineItemViewHolder separatorLineItemViewHolder;
    private PlaceViewHolder placeViewHolder;

    public PlaceAutocompleteAdapter(Context context, ArrayList<PlaceAutocompleteModel> placeAutocompleteModels){
        this.mContext = context;
        this.placeAutocompleteModels = placeAutocompleteModels;
    }
    public void setmListener(NavigationFragment mListener) {
        this.autoCompleteItemInterface = mListener;
    }

    @Override
    public RecyclerView.ViewHolder  onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == GPS){
            itemView = LayoutInflater.from(parent.getContext()).inflate( R.layout.search_places_item, null);
            return new PlaceViewHolder(itemView);
        }
        else if (viewType == SEARCH){
            itemView = LayoutInflater.from(parent.getContext()).inflate( R.layout.search_places_item, null);
            return new PlaceViewHolder(itemView);
        }
        else if (viewType == SEPARATORLINE){
            itemView = LayoutInflater.from(parent.getContext()).inflate( R.layout.separator_line_item, null);
           return new SeparatorLineItemViewHolder(itemView);
        }
        else{
            itemView = LayoutInflater.from(parent.getContext()).inflate( R.layout.separator_line_item, null);
            return new SeparatorLineItemViewHolder(itemView);
        }
    }

    private class PlaceViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout addressRl;
        public ImageView addressImg;
        public TextView addressFullTv,addressTypeTv;

        public PlaceViewHolder(View itemView) {
            super(itemView);
            addressRl = (RelativeLayout)itemView.findViewById(R.id.addressRl);
            addressFullTv = (TextView)itemView.findViewById(R.id.addressFullTv);
            addressTypeTv = (TextView)itemView.findViewById(R.id.addressTypeTv);
            addressImg = itemView.findViewById(R.id.addressImg);
            addressRl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    autoCompleteItemInterface.onPlaceItemClick(getAdapterPosition());
                }
            });

        }

    }

    private class SeparatorLineItemViewHolder extends RecyclerView.ViewHolder {
        SeparatorLineItemViewHolder(View v) {
            super(v);

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        PlaceAutocompleteModel placeAutocompleteModel = placeAutocompleteModels.get(position);
        Integer layoutType = placeAutocompleteModel.getLayoutType();
        if(layoutType == GPS) {
            placeViewHolder = (PlaceViewHolder) holder;
            placeViewHolder.addressImg.setImageResource(R.drawable.gps_icon);
            placeViewHolder.addressTypeTv.setText(placeAutocompleteModel.getDescription());
            placeViewHolder.addressFullTv.setText(placeAutocompleteModel.getFulltext());
        }
        if(layoutType == SEARCH) {
            placeViewHolder = (PlaceViewHolder) holder;
            placeViewHolder.addressTypeTv.setText(placeAutocompleteModel.getDescription());
            placeViewHolder.addressFullTv.setText(placeAutocompleteModel.getFulltext());
        }
        if(layoutType == SEPARATORLINE) {

        }
    }
    @Override
    public int getItemCount() {
        return placeAutocompleteModels.size();
    }
    public int getItemViewType(int position) {
        return placeAutocompleteModels.get(position).getLayoutType();
    }
}