package com.gratus.idp.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.gratus.idp.R;
import com.gratus.idp.databinding.FragmentCommunityBinding;
import com.gratus.idp.databinding.FragmentNewsFeedBinding;
import com.gratus.idp.model.common.ChallengeEvent;
import com.gratus.idp.model.common.CommunityModel;
import com.gratus.idp.model.common.NewsFeedModel;
import com.gratus.idp.view.activity.ChallengeActivity;
import com.gratus.idp.view.activity.EventActivity;
import com.gratus.idp.view.activity.LeaderActivity;
import com.gratus.idp.view.activity.NewsFeedActivity;
import com.gratus.idp.view.adapter.ChallengeEventAdapter;
import com.gratus.idp.view.adapter.CommunityAdapter;
import com.gratus.idp.view.adapter.NewsFeedAdapter;
import com.gratus.idp.view.base.BaseFragement;
import com.gratus.idp.view.interfaces.adapter.CommunityListListener;
import com.gratus.idp.view.interfaces.adapter.NewsFeedListListener;
import com.gratus.idp.viewModel.fragment.CommunityViewModel;
import com.gratus.idp.viewModel.fragment.NewsFeedViewModel;

import java.io.Serializable;

import javax.inject.Inject;

public class CommunityFragment extends BaseFragement implements CommunityListListener {

    private FragmentCommunityBinding fragmentCommunityBinding;
    private View mRootView;
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private CommunityViewModel communityViewModel;
    @Inject
    CommunityAdapter communityAdapter;
    @Inject
    LinearLayoutManager mLayoutManager;

    public CommunityFragment() {
    }

    public static CommunityFragment newInstance(String param1, String param2) {
        CommunityFragment fragment = new CommunityFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        communityViewModel = ViewModelProviders.of(this, viewModelFactory).get(CommunityViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentCommunityBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_community, container, false);
        mRootView = fragmentCommunityBinding.getRoot();
        fragmentCommunityBinding.setCommunityViewModel(communityViewModel);
        fragmentCommunityBinding.setLifecycleOwner(this);
        communityAdapter.setmListener(this);
        communityViewModel.setNewsFeedItems();
        setNewsUp();
        updateReport();
        return mRootView;
    }

    private void setNewsUp() {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        fragmentCommunityBinding.recView.setLayoutManager(mLayoutManager);
        fragmentCommunityBinding.recView.setItemAnimator(new DefaultItemAnimator());
        fragmentCommunityBinding.recView.setAdapter(communityAdapter);
    }


    private void updateReport() {
        communityAdapter.addItems(communityViewModel.getCommunityModels());
    }


    @Override
    public void onItemClick(CommunityModel communityModel) {
        Intent intent = new Intent(getActivity(), LeaderActivity.class);
        startActivity(intent);
    }

    @Override
    public void onSubItemClick(ChallengeEvent challengeEvent) {
        if (challengeEvent.getLayoutId() == 1) {
            Intent intent = new Intent(getActivity(), ChallengeActivity.class);
            intent.putExtra("challengeEvent", (Serializable) challengeEvent);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getActivity(), EventActivity.class);
            intent.putExtra("challengeEvent", (Serializable) challengeEvent);
            startActivity(intent);
        }
    }
}
