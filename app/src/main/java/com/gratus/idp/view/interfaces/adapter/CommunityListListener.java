package com.gratus.idp.view.interfaces.adapter;

import com.gratus.idp.model.common.ChallengeEvent;
import com.gratus.idp.model.common.CommunityModel;
import com.gratus.idp.model.common.NewsFeedModel;

public interface CommunityListListener {
    void onItemClick(CommunityModel position);
    void onSubItemClick(ChallengeEvent position);

}
