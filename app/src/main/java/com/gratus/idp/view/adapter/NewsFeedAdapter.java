package com.gratus.idp.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gratus.idp.databinding.ItemNewsfeedListBinding;
import com.gratus.idp.databinding.ItemReportListBinding;
import com.gratus.idp.databinding.ItemTempNewsfeedListBinding;
import com.gratus.idp.model.common.CyclePathNew;
import com.gratus.idp.model.common.NewsFeedModel;
import com.gratus.idp.model.common.Properties;
import com.gratus.idp.view.base.BaseViewHolder;
import com.gratus.idp.view.fragment.NewsFeedFragment;
import com.gratus.idp.view.interfaces.adapter.NewsFeedListListener;
import com.gratus.idp.view.interfaces.adapter.ReportListListener;
import com.gratus.idp.view.interfaces.viewModel.NewsFeedListItemViewModelListener;
import com.gratus.idp.view.interfaces.viewModel.ReportListItemViewModelListener;
import com.gratus.idp.viewModel.adapter.NewsFeedItemListViewModel;
import com.gratus.idp.viewModel.adapter.RepostItemListViewModel;
import com.gratus.idp.viewModel.fragment.NewsFeedViewModel;

import java.util.ArrayList;
import java.util.List;

public class NewsFeedAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_NORMAL = 1;
    public static final int VIEW_TYPE_TEMP = 2;
    public static final int VIEW_TYPE_EMPTY = 0;
    private NewsFeedListListener mListener;
    private ArrayList<NewsFeedModel> newsFeedModels;

    public NewsFeedAdapter(ArrayList<NewsFeedModel> newsFeedModels) {
        this.newsFeedModels = newsFeedModels;
    }

    public void setmListener(NewsFeedFragment mListener) {
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                 ItemNewsfeedListBinding itemNewsfeedListBinding = ItemNewsfeedListBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new NewsFeedListViewHolder(itemNewsfeedListBinding);
            case VIEW_TYPE_TEMP:
                ItemTempNewsfeedListBinding itemTempNewsfeedListBinding = ItemTempNewsfeedListBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new NewsFeedTempListViewHolder(itemTempNewsfeedListBinding);
            case VIEW_TYPE_EMPTY:
            default:
                itemNewsfeedListBinding = ItemNewsfeedListBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new NewsFeedListViewHolder(itemNewsfeedListBinding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
            return newsFeedModels.size();
    }

    @Override
    public int getItemViewType(int position) {
            return newsFeedModels.get(position).getLayoutId();
    }

    public void clearItems() {
        newsFeedModels.clear();
    }

    public void addItems(ArrayList<NewsFeedModel> newsFeedModel) {
        newsFeedModels.clear();
        newsFeedModels.addAll(newsFeedModel);
        notifyDataSetChanged();
    }

    private class NewsFeedListViewHolder extends BaseViewHolder implements NewsFeedListItemViewModelListener {
        private ItemNewsfeedListBinding mBinding;
        private NewsFeedItemListViewModel newsFeedItemListViewModel;

        public NewsFeedListViewHolder(ItemNewsfeedListBinding itemNewsfeedListBinding) {
            super(itemNewsfeedListBinding.getRoot());
            this.mBinding = itemNewsfeedListBinding;
        }

        @Override
        public void onBind(int position) {
            final NewsFeedModel newsFeedModel = newsFeedModels.get(position);
            newsFeedItemListViewModel = new NewsFeedItemListViewModel(newsFeedModel, this);
            mBinding.setNewsFeedItemListViewModel(newsFeedItemListViewModel);
        }
        @Override
        public void onItemClick() {
            mListener.onItemClick(newsFeedModels.get(getAdapterPosition()));
        }
    }

    private class NewsFeedTempListViewHolder extends BaseViewHolder {
        private ItemTempNewsfeedListBinding mBinding;
        private NewsFeedItemListViewModel newsFeedItemListViewModel;

        public NewsFeedTempListViewHolder(ItemTempNewsfeedListBinding itemTempNewsfeedListBinding) {
            super(itemTempNewsfeedListBinding.getRoot());
            this.mBinding = itemTempNewsfeedListBinding;
        }

        @Override
        public void onBind(int position) {
            final NewsFeedModel newsFeedModel = newsFeedModels.get(position);
            newsFeedItemListViewModel = new NewsFeedItemListViewModel(newsFeedModel);
            mBinding.setNewsFeedItemListViewModel(newsFeedItemListViewModel);

        }
    }
}
