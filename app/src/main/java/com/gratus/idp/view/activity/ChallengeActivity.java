package com.gratus.idp.view.activity;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.gratus.idp.R;
import com.gratus.idp.databinding.ActivityChallengeBinding;
import com.gratus.idp.model.common.ChallengeEvent;
import com.gratus.idp.view.base.BaseActivity;
import com.gratus.idp.viewModel.activity.ChallengeViewModel;

import javax.inject.Inject;

public class ChallengeActivity extends BaseActivity {
    private ActivityChallengeBinding activityChallengeBinding;
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private ChallengeViewModel challengeViewModel;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityChallengeBinding = DataBindingUtil.setContentView(this, R.layout.activity_challenge);
        //  ((BaseApplication) getApplicationContext()).getAppComponent().inject(this);
        challengeViewModel = ViewModelProviders.of(this, viewModelFactory).get(ChallengeViewModel.class);
        activityChallengeBinding.setChallengeViewModel(challengeViewModel);
        activityChallengeBinding.setLifecycleOwner(this);
        activityChallengeBinding.backArrowImg.setOnClickListener(v -> onBackPressed());
        challengeViewModel.setChallengeEventl((ChallengeEvent) getIntent().getSerializableExtra("challengeEvent"));
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected, activityChallengeBinding.parent);
        setIntial(false);
    }
}
