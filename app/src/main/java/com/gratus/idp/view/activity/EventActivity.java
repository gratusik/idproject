package com.gratus.idp.view.activity;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.gratus.idp.R;
import com.gratus.idp.databinding.ActivityEventBinding;
import com.gratus.idp.model.common.ChallengeEvent;
import com.gratus.idp.view.base.BaseActivity;
import com.gratus.idp.viewModel.activity.ChallengeViewModel;
import com.gratus.idp.viewModel.activity.EventViewModel;

import javax.inject.Inject;

public class EventActivity extends BaseActivity {
    private ActivityEventBinding activityEventBinding;
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private EventViewModel eventViewModel;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityEventBinding = DataBindingUtil.setContentView(this, R.layout.activity_event);
        //  ((BaseApplication) getApplicationContext()).getAppComponent().inject(this);
        eventViewModel = ViewModelProviders.of(this, viewModelFactory).get(EventViewModel.class);
        activityEventBinding.setEventViewModel(eventViewModel);
        activityEventBinding.setLifecycleOwner(this);
        activityEventBinding.backArrowImg.setOnClickListener(v -> onBackPressed());
        eventViewModel.setChallengeEventl((ChallengeEvent) getIntent().getSerializableExtra("challengeEvent"));
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected, activityEventBinding.parent);
        setIntial(false);
    }
}
