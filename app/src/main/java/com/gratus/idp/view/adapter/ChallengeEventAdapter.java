package com.gratus.idp.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gratus.idp.databinding.ItemChallengeEventBinding;
import com.gratus.idp.databinding.ItemCommunityChallengeBinding;
import com.gratus.idp.databinding.ItemCommunityEventsBinding;
import com.gratus.idp.databinding.ItemNewsfeedListBinding;
import com.gratus.idp.databinding.ItemTempNewsfeedListBinding;
import com.gratus.idp.model.common.ChallengeEvent;
import com.gratus.idp.model.common.ChallengeEvent;
import com.gratus.idp.view.base.BaseViewHolder;
import com.gratus.idp.view.fragment.CommunityFragment;
import com.gratus.idp.view.fragment.NewsFeedFragment;
import com.gratus.idp.view.interfaces.adapter.CommunityListListener;
import com.gratus.idp.view.interfaces.adapter.NewsFeedListListener;
import com.gratus.idp.view.interfaces.viewModel.CommunityCEItemViewModelListener;
import com.gratus.idp.view.interfaces.viewModel.NewsFeedListItemViewModelListener;
import com.gratus.idp.viewModel.adapter.CommunityChallengeEventItemListViewModel;

import java.util.ArrayList;

public class ChallengeEventAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_CHALLENGE = 1;
    public static final int VIEW_TYPE_EVENTS = 2;
    private CommunityListListener mListener;
    private ArrayList<ChallengeEvent> challengeEvents;

    public ChallengeEventAdapter(ArrayList<ChallengeEvent> challengeEvents) {
        this.challengeEvents = challengeEvents;
    }

    public ChallengeEventAdapter(CommunityListListener mListener, ArrayList<ChallengeEvent> challengeEvent) {
        this.mListener = mListener;
        this.challengeEvents = challengeEvent;
    }

    public void setmListener(CommunityFragment mListener) {
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_CHALLENGE:
                 ItemCommunityChallengeBinding itemCommunityChallengeBinding = ItemCommunityChallengeBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new CommunityChallengeListViewHolder(itemCommunityChallengeBinding);
            case VIEW_TYPE_EVENTS:
                ItemCommunityEventsBinding itemCommunityEventsBinding= ItemCommunityEventsBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new CommunityEventListViewHolder(itemCommunityEventsBinding);
            default:
                 itemCommunityChallengeBinding = ItemCommunityChallengeBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new CommunityChallengeListViewHolder(itemCommunityChallengeBinding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
            return challengeEvents.size();
    }

    @Override
    public int getItemViewType(int position) {
            return challengeEvents.get(position).getLayoutId();
    }

    public void clearItems() {
        challengeEvents.clear();
    }

    public void addItems(ArrayList<ChallengeEvent> challengeEvent) {
        challengeEvents.clear();
        challengeEvents.addAll(challengeEvent);
        notifyDataSetChanged();
    }

    private class CommunityChallengeListViewHolder extends BaseViewHolder implements CommunityCEItemViewModelListener {
        private ItemCommunityChallengeBinding mBinding;
        private CommunityChallengeEventItemListViewModel communityChallengeEventItemListViewModel;

        public CommunityChallengeListViewHolder(ItemCommunityChallengeBinding itemCommunityChallengeBinding) {
            super(itemCommunityChallengeBinding.getRoot());
            this.mBinding = itemCommunityChallengeBinding;
        }

        @Override
        public void onBind(int position) {
            final ChallengeEvent challengeEvent = challengeEvents.get(position);
            communityChallengeEventItemListViewModel = new CommunityChallengeEventItemListViewModel(challengeEvent, this);
            mBinding.setCommunityChallengeEventItemListViewModel(communityChallengeEventItemListViewModel);
        }
        @Override
        public void onItemClick() {
            mListener.onSubItemClick(challengeEvents.get(getAdapterPosition()));
        }
    }

    private class CommunityEventListViewHolder extends BaseViewHolder implements CommunityCEItemViewModelListener {
        private ItemCommunityEventsBinding mBinding;
        private CommunityChallengeEventItemListViewModel communityChallengeEventItemListViewModel;

        public CommunityEventListViewHolder(ItemCommunityEventsBinding itemCommunityEventsBinding) {
            super(itemCommunityEventsBinding.getRoot());
            this.mBinding = itemCommunityEventsBinding;
        }

        @Override
        public void onBind(int position) {
            final ChallengeEvent challengeEvent = challengeEvents.get(position);
            communityChallengeEventItemListViewModel = new CommunityChallengeEventItemListViewModel(challengeEvent,this);
            mBinding.setCommunityChallengeEventItemListViewModel(communityChallengeEventItemListViewModel);
        }
        @Override
        public void onItemClick() {
            mListener.onSubItemClick(challengeEvents.get(getAdapterPosition()));
        }
    }
}
