package com.gratus.idp.view.interfaces.viewModel;

public interface CommunityLeaderItemViewModelListener {
    void onItemClick();
}
