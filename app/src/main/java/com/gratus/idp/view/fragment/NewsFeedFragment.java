package com.gratus.idp.view.fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.gratus.idp.R;
import com.gratus.idp.databinding.FragmentNewsFeedBinding;
import com.gratus.idp.databinding.FragmentReportListingBinding;
import com.gratus.idp.model.common.CyclePathNew;
import com.gratus.idp.model.common.NewsFeedModel;
import com.gratus.idp.model.response.OutputCycle;
import com.gratus.idp.view.activity.NewsFeedActivity;
import com.gratus.idp.view.activity.PathActivity;
import com.gratus.idp.view.adapter.NewsFeedAdapter;
import com.gratus.idp.view.adapter.ReportListAdapter;
import com.gratus.idp.view.interfaces.adapter.NewsFeedListListener;
import com.gratus.idp.view.interfaces.adapter.ReportListListener;
import com.gratus.idp.view.base.BaseFragement;
import com.gratus.idp.viewModel.fragment.NewsFeedViewModel;
import com.gratus.idp.viewModel.fragment.ReportListViewModel;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;

import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import static com.gratus.idp.util.constants.AppConstantsCode.NETWORK_CODE_EXP;

public class NewsFeedFragment extends BaseFragement implements NewsFeedListListener {

    private FragmentNewsFeedBinding fragmentNewsFeedBinding;
    private View mRootView;
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private NewsFeedViewModel newsFeedViewModel;
    @Inject
    NewsFeedAdapter newsFeedAdapter;
    @Inject
    LinearLayoutManager mLayoutManager;

    public NewsFeedFragment() {
    }

    public static NewsFeedFragment newInstance(String param1, String param2) {
        NewsFeedFragment fragment = new NewsFeedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        newsFeedViewModel = ViewModelProviders.of(this, viewModelFactory).get(NewsFeedViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentNewsFeedBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_news_feed, container, false);
        mRootView = fragmentNewsFeedBinding.getRoot();
        fragmentNewsFeedBinding.setNewsFeedViewModel(newsFeedViewModel);
        fragmentNewsFeedBinding.setLifecycleOwner(this);
        newsFeedAdapter.setmListener(this);
        newsFeedViewModel.setNewsFeedItems();
        setNewsUp();
        updateReport();
        return mRootView;
    }

    private void setNewsUp() {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        fragmentNewsFeedBinding.recView.setLayoutManager(mLayoutManager);
        fragmentNewsFeedBinding.recView.setItemAnimator(new DefaultItemAnimator());
        fragmentNewsFeedBinding.recView.setAdapter(newsFeedAdapter);
    }


    private void updateReport() {

        newsFeedAdapter.addItems(newsFeedViewModel.getNewsFeedModel());
    }


    @Override
    public void onItemClick(NewsFeedModel newsFeedModel) {
        Intent intent = new Intent(getActivity(), NewsFeedActivity.class);
        intent.putExtra("newsFeedModel", (Serializable) newsFeedModel);
        startActivity(intent);
    }
}
