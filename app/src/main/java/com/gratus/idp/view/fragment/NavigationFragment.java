package com.gratus.idp.view.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.gratus.idp.R;
import com.gratus.idp.databinding.FragmentNavigationBinding;
import com.gratus.idp.databinding.FragmentRideEarnBinding;
import com.gratus.idp.model.common.PlaceAutocompleteModel;
import com.gratus.idp.util.GMapV2Direction;
import com.gratus.idp.view.adapter.PlaceAutocompleteAdapter;
import com.gratus.idp.view.adapter.RideEarnAdapter;
import com.gratus.idp.view.base.BaseFragement;
import com.gratus.idp.view.interfaces.adapter.AutoCompleteItemInterface;
import com.gratus.idp.viewModel.fragment.NavigationViewModel;
import com.gratus.idp.viewModel.fragment.RideEarnViewModel;

import org.w3c.dom.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.inject.Inject;

import static android.content.ContentValues.TAG;
import static android.content.Context.LOCATION_SERVICE;

public class NavigationFragment extends BaseFragement implements
        AutoCompleteItemInterface, OnMapReadyCallback{
    private FragmentNavigationBinding fragmentNavigationBinding;
    private View mRootView;
    private GoogleMap mMap;
    private int GPS = 3;
    private int SEARCH = 4;
    private int SEPARATORLINE = 7;
    boolean from = false;
    boolean to  = false;
    private LatLng mDefaultLocation = new LatLng(45.054692, 7.659687); //should be restaurant location, by default
    private LatLng mClientLocation = new LatLng(45.062450,7.662360); // studen
    private static final int DEFAULT_ZOOM = 15;
    private PlaceAutocompleteAdapter mSearchAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<PlaceAutocompleteModel> placeAutocompleteModels = new ArrayList();
    PlacesClient placesClient;
    private AutoCompleteItemInterface autoCompleteItemInterface;
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private NavigationViewModel navigationViewModel;
    String apiKey = "AIzaSyANii2Z2nzHvvD5tn_qB4HTCnAdgwJRFHY";
    private LocationManager mLocationManager;
    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    LatLng fromlatLong;
    LatLng tolatLong;
    private boolean mLocationPermissionGranted = true;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    private LocationCallback locationCallback;
    private GMapV2Direction md;
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;
    private Location mLastKnownLocation;
    public NavigationFragment() {
    }

    public static NavigationFragment newInstance(String param1, String param2) {
        NavigationFragment fragment = new NavigationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        navigationViewModel = ViewModelProviders.of(this, viewModelFactory).get(NavigationViewModel.class);
        mLocationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                MIN_DISTANCE_CHANGE_FOR_UPDATES, mLocationListener);
        if (mLocationManager != null) {
            location = mLocationManager
                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }
        }
        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    // Update UI with location data
                    // ...
                }
            };
        };
    }
    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentNavigationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_navigation, container, false);
        mRootView = fragmentNavigationBinding.getRoot();
        fragmentNavigationBinding.setNavigationViewModel(navigationViewModel);
        fragmentNavigationBinding.setLifecycleOwner(this);
        fragmentNavigationBinding.map.onCreate(savedInstanceState);
        fragmentNavigationBinding.map.onResume();
        fragmentNavigationBinding.map.getMapAsync(this);
        md = new GMapV2Direction(getResources().getString(R.string.google_api_key), getContext());
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), apiKey);
        }
        placesClient = Places.createClient(getContext());
        getAddress();
        fragmentNavigationBinding.fromEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    if (mSearchAdapter != null) {
                        fragmentNavigationBinding.searchdRecyclerView.setVisibility(View.VISIBLE);
                    }
                } else {
                    placeAutocompleteModels.add(new PlaceAutocompleteModel(GPS,"1","Current Location","Using GPS"));
                    setSearchListAdapter();
                }
                if (!s.toString().equals("")) {
                    fragmentNavigationBinding.mapLayout.setVisibility(View.GONE);
                    fragmentNavigationBinding.searchdRecyclerView.setVisibility(View.VISIBLE);
                    getAutocomplete(s.toString());
                    from = true;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        fragmentNavigationBinding.toET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    if (mSearchAdapter != null) {
                        fragmentNavigationBinding.searchdRecyclerView.setVisibility(View.VISIBLE);
                    }
                } else {
                    placeAutocompleteModels.add(new PlaceAutocompleteModel(GPS,"1","Current Location","Using GPS"));
                    setSearchListAdapter();
                }
                if (!s.toString().equals("")) {
                    fragmentNavigationBinding.mapLayout.setVisibility(View.GONE);
                    fragmentNavigationBinding.searchdRecyclerView.setVisibility(View.VISIBLE);
                    getAutocomplete(s.toString());
                    to = true;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return mRootView;
    }
    private void getAddress() {
        placeAutocompleteModels.add(new PlaceAutocompleteModel(GPS,"1","Current Location","Using GPS"));
        setSearchListAdapter();
    }

    private void setSearchListAdapter() {
        mLayoutManager = new LinearLayoutManager(getContext());
        mSearchAdapter = new PlaceAutocompleteAdapter(getContext(),placeAutocompleteModels);
        fragmentNavigationBinding.searchdRecyclerView.setLayoutManager(mLayoutManager);
        fragmentNavigationBinding.searchdRecyclerView.setAdapter(mSearchAdapter);
        mSearchAdapter.notifyDataSetChanged();
        mSearchAdapter.setmListener(this);
    }

    public void clearList(){
        if(placeAutocompleteModels!=null && placeAutocompleteModels.size()>0){
            placeAutocompleteModels.clear();
        }
    }

    private void getAutocomplete(CharSequence constraint) {
//        ArrayList<PlaceAutocompleteModel> placeAutocompleteModel =  new ArrayList<>();
//        placeAutocompleteModel.add(new PlaceAutocompleteModel(SEARCH,"2","University Residence Borsellino","University Residence Borsellino",45.0662,7.6571));
//        placeAutocompleteModel.add(new PlaceAutocompleteModel(SEARCH,"3","Alloggiami Student Housing | Tecla Zaia","Alloggiami Student Housing | Tecla Zaia",45.020795,7.611116));
//        placeAutocompleteModel.add(new PlaceAutocompleteModel(SEARCH,"4","FCA Heritage Hub","FCA Heritage Hub",45.022978,7.610650));
//        placeAutocompleteModel.add(new PlaceAutocompleteModel(SEARCH,"5","Egyptian Museu","Egyptian Museum",45.068545, 7.684387));
//        placeAutocompleteModel.add(new PlaceAutocompleteModel(SEARCH,"6","Stupinigi (Castello)","Stupinigi (Castello)",44.997734, 7.607196));
//        placeAutocompleteModel.add(new PlaceAutocompleteModel(SEARCH,"6","Porta susa","Porta susa",45.071596, 7.666011));
//        placeAutocompleteModels.clear();
//        placeAutocompleteModels.addAll(placeAutocompleteModel);
//        placeAutocompleteModels.add(new PlaceAutocompleteModel(GPS,"1","Via Verzuola","Via Verzuola",45.066337, 7.647209));
//        if(mSearchAdapter!=null) {
//                        mSearchAdapter.notifyDataSetChanged();
//                    }
        AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();

        FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                .setSessionToken(token)
                .setQuery(constraint.toString())
                .build();
        placesClient.findAutocompletePredictions(request).addOnSuccessListener((response) -> {
            ArrayList<PlaceAutocompleteModel> placeAutocompleteModel =  new ArrayList<>();
            for (AutocompletePrediction prediction : response.getAutocompletePredictions()) {
                String placid = prediction.getPlaceId();
                String primarytext =prediction.getPrimaryText(null).toString();
                String fulltext =prediction.getFullText(null).toString();

                placeAutocompleteModel.add(new PlaceAutocompleteModel(SEARCH,placid,primarytext,fulltext));
                placeAutocompleteModel.add(new PlaceAutocompleteModel(SEPARATORLINE));
                if(placeAutocompleteModel.size()>0){
                    placeAutocompleteModels.clear();
                    placeAutocompleteModels.addAll(placeAutocompleteModel);
                    placeAutocompleteModels.add(new PlaceAutocompleteModel(GPS,"1","Current Location","Using GPS"));
                    if(mSearchAdapter!=null) {
                        mSearchAdapter.notifyDataSetChanged();
                    }
                    else{
                        setSearchListAdapter();
                    }
                }
            }
        }).addOnFailureListener((exception) -> {
            if (exception instanceof ApiException) {
                ApiException apiException = (ApiException) exception;
                clearList();
                fragmentNavigationBinding.searchdRecyclerView.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onPlaceItemClick(int adapterPosition) {
        Geocoder geocoder = new Geocoder(getContext());
        List<Address> addresses = null;
        double latitude = 0;
        double longitude = 0;
        String add = placeAutocompleteModels.get(adapterPosition).getFulltext().toString();
        try {
            addresses = geocoder.getFromLocationName(add, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses.size() > 0) {
            latitude = addresses.get(0).getLatitude();
            longitude = addresses.get(0).getLongitude();
            Log.i("lat", latitude + "");
            Log.i("long", longitude + "");
        }
        if (from) {
            fragmentNavigationBinding.fromEt.setText(placeAutocompleteModels.get(adapterPosition).getDescription());
            from = false;
            fragmentNavigationBinding.searchdRecyclerView.setVisibility(View.GONE);
             fromlatLong = new LatLng(latitude,longitude);
        }
        if (to) {
            to = false;
            fragmentNavigationBinding.toET.setText(placeAutocompleteModels.get(adapterPosition).getDescription());
            fragmentNavigationBinding.searchdRecyclerView.setVisibility(View.GONE);
            tolatLong = new LatLng(latitude,longitude);
        }
        if(fragmentNavigationBinding.fromEt.getText().length()>3 &&fragmentNavigationBinding.toET.getText().length()>3){
            fragmentNavigationBinding.mapLayout.setVisibility(View.VISIBLE);
           getDeviceLocation();
        }
//        if(placeAutocompleteModels.get(adapterPosition).getLayoutType()==GPS){
//            Intent i = new Intent();
//            setResult(Activity.RESULT_OK,i);
//            finish();
//            ((Activity)context_main).overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
//        }
//        if(placeAutocompleteModels.get(adapterPosition).getLayoutType()==SEARCH){
//            Intent i = new Intent();
//            i.putExtra("latitude", latitude+"");
//            i.putExtra("longitude", longitude+"");
//            setResult(Activity.RESULT_OK,i);
//            finish();
//            ((Activity)context_main).overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
//        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng latLng = new LatLng(latitude, longitude);
        mMap = googleMap;
        MarkerOptions markerOptions = new MarkerOptions().position(latLng);
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        googleMap.addMarker(markerOptions);
       // getDeviceLocation();
    }
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            //mLastKnownLocation = task.getResult();
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(fromlatLong.latitude,
                                            fromlatLong.longitude), DEFAULT_ZOOM));
                            //String location = String.valueOf(mLastKnownLocation.getLatitude())+"°, "+String.valueOf(mLastKnownLocation.getLongitude())+"°";
                           // Log.d("location",location);

//                            LatLng mLastKnownLatLng = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                            String[] direction = {String.valueOf(fromlatLong.latitude), String.valueOf(fromlatLong.longitude), String.valueOf(tolatLong.latitude), String.valueOf(tolatLong.longitude),GMapV2Direction.MODE_DRIVING};
                            Document doc = null;
                            try {
                                doc = md.execute(direction).get();
                            } catch (ExecutionException e) {
                                e.printStackTrace();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            if (doc == null){
                                Log.d("doc is null", String.valueOf(true));
                            } else {
                                ArrayList<LatLng> directionPoint = md.getDirection(doc);
                                PolylineOptions rectLine = new PolylineOptions().width(10).color(getContext().getResources().getColor(R.color.green));
                                Log.v("nb steps", String.valueOf(directionPoint.size()));
                                int half = ((directionPoint.size()/2));
                                for (int i = 0; i < half; i++) {
                                    rectLine.add(directionPoint.get(i));
                                }
                                Polyline polyline = mMap.addPolyline(rectLine);
                                rectLine = new PolylineOptions().width(10).color(getContext().getResources().getColor(R.color.red));
                                for (int i = half; i < directionPoint.size(); i++) {
                                    rectLine.add(directionPoint.get(i));
                                }
                                Polyline polyline1 = mMap.addPolyline(rectLine);
                            }
                            MarkerOptions markerOptions = new MarkerOptions().position(tolatLong);
                            mMap.addMarker(markerOptions);
                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            mMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }
    public void onMapReady() {
        LatLng latLng = new LatLng(latitude, longitude);
        MarkerOptions markerOptions = new MarkerOptions().position(latLng);
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        mMap.addMarker(markerOptions);
    }
}
