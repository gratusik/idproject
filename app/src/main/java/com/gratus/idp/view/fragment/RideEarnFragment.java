package com.gratus.idp.view.fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.gratus.idp.R;
import com.gratus.idp.databinding.FragmentHomeBinding;
import com.gratus.idp.databinding.FragmentRideEarnBinding;
import com.gratus.idp.generated.callback.OnClickListener;
import com.gratus.idp.model.common.CyclePathNew;
import com.gratus.idp.model.common.MarkerArray;
import com.gratus.idp.model.common.MarkerArrayPath;
import com.gratus.idp.model.common.PolylineModel;
import com.gratus.idp.model.response.OutputCycle;
import com.gratus.idp.view.adapter.NewsFeedAdapter;
import com.gratus.idp.view.adapter.RideEarnAdapter;
import com.gratus.idp.view.base.BaseFragement;
import com.gratus.idp.viewModel.fragment.HomeViewModel;
import com.gratus.idp.viewModel.fragment.RideEarnViewModel;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import static android.content.Context.LOCATION_SERVICE;
import static com.gratus.idp.util.constants.AppConstantsCode.NETWORK_CODE_EXP;

public class RideEarnFragment extends BaseFragement implements OnMapReadyCallback{
    private FragmentRideEarnBinding fragmentRideEarnBinding;
    private View mRootView;
    private GoogleMap mMap;
    @Inject
    RideEarnAdapter rideEarnAdpater;
    @Inject
    LinearLayoutManager mLayoutManager;
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private RideEarnViewModel rideEarnViewModel;

    private LocationManager mLocationManager;
    Location location; // location
    double latitude; // latitude
    double longitude; // longitude

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

    public RideEarnFragment() {
    }

    public static RideEarnFragment newInstance(String param1, String param2) {
        RideEarnFragment fragment = new RideEarnFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rideEarnViewModel = ViewModelProviders.of(this, viewModelFactory).get(RideEarnViewModel.class);
        mLocationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                MIN_DISTANCE_CHANGE_FOR_UPDATES, mLocationListener);
        if (mLocationManager != null) {
            location = mLocationManager
                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }
        }
    }
    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            onMapReady();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentRideEarnBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_ride_earn, container, false);
        mRootView = fragmentRideEarnBinding.getRoot();
        fragmentRideEarnBinding.setRideEarnViewModel(rideEarnViewModel);
        fragmentRideEarnBinding.setLifecycleOwner(this);
        fragmentRideEarnBinding.map.onCreate(savedInstanceState);
        fragmentRideEarnBinding.map.onResume();
        fragmentRideEarnBinding.map.getMapAsync(this);
        rideEarnViewModel.setNewsFeedItems();
        setNewsUp();
        updateReport();
        fragmentRideEarnBinding.goBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fragmentRideEarnBinding.offlineTv.getText().equals("You're Offline"))
                {
                    fragmentRideEarnBinding.offlineTv.setText("You're Online");
                }
                else{
                    fragmentRideEarnBinding.offlineTv.setText("You're Offline");
                }
            }
        });

        fragmentRideEarnBinding.bottomRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fragmentRideEarnBinding.recView.getVisibility()==View.GONE) {
                    fragmentRideEarnBinding.recView.setVisibility(View.VISIBLE);
                }
                else{
                    fragmentRideEarnBinding.recView.setVisibility(View.GONE);
                }
            }
        });
        return mRootView;
    }

    public void onMapReady() {
        LatLng latLng = new LatLng(latitude, longitude);
        MarkerOptions markerOptions = new MarkerOptions().position(latLng);
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        mMap.addMarker(markerOptions);
    }
    private void setNewsUp() {
        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mLayoutManager.setReverseLayout(true);
        fragmentRideEarnBinding.recView.setLayoutManager(mLayoutManager);
        fragmentRideEarnBinding.recView.setItemAnimator(new DefaultItemAnimator());
        fragmentRideEarnBinding.recView.setAdapter(rideEarnAdpater);
    }


    private void updateReport() {

        rideEarnAdpater.addItems(rideEarnViewModel.getRideEarnModels());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng latLng = new LatLng(latitude, longitude);
        mMap = googleMap;
        MarkerOptions markerOptions = new MarkerOptions().position(latLng);
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        googleMap.addMarker(markerOptions);
    }
}
